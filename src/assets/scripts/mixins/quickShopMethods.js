/**
 * Quick Shop methods
 * -----------------------------------------------------------------------------
 *
 * @namespace quickShopMethods
 */

import axios from 'axios';
import Flickity from 'flickity';
import 'flickity-bg-lazyload';
import detectIt from 'detect-it';

const quickShopMethods = {

  methods: {

    /**
     * Adds a variant from wishlist Array
     * @param  {productHandle}
     *
     */
    _viewQuickShop(productHandle, optionValue) {
      const $modal = $(app.selectors.quickShopContainer);
      const $modalBody = $modal.find('.modal-body');
      const $qtyEl = $modal.find(app.selectors.qtyInput);
      let $carouselMain;
      let optionsForMain;
      let flktyMain;
      app.isLoading = true;


      axios.get(`/products/${productHandle}.js`)
        .then((response) => {
          app.quickShop.product = response.data;
        })
        .catch((error) => {
          // console.log(error)
        });

      axios.get(`/products/${productHandle}?view=json`)
        .then((response) => {
          app.isLoading = false;
          app.quickShop.images = response.data.images;
          app.quickShop.optionImages = response.data.option_images;
          $modal.modal('show');

          $modal.on('shown.bs.modal', function(e) {
            app.isQuickshopModalOpen = true;
            $carouselMain = $(this).find('.carousel-main');

            // re-init tooltips but only for desktop
            if (!detectIt.hasTouch) {
              $('[data-toggle="tooltip"]').tooltip();
            }

            if ($carouselMain.length > 0) {
              optionsForMain = $carouselMain.data('flickity');
              flktyMain = new Flickity($carouselMain[0], optionsForMain);
              flktyMain.resize();
            }

            $(`input[value="${optionValue}"]`).trigger('click');

            app._getVariant(optionValue);
            $modalBody.addClass('opened');
          });

          $modal.on('hidden.bs.modal', (e) => {
            $qtyEl.val(1); // reset qty input to 1
            app.quickShop.product = {};
            app.quickShop.images = {};
            app.quickShop.currentVariant = {};
            app.isQuickshopModalOpen = false;

            $modalBody.removeClass('opened');

            if ($carouselMain.length > 0) {
              flktyMain.destroy();
            }
          });
        })
        .catch((error) => {
          // console.log(error)
        });

    },

  },
};

export default quickShopMethods;
