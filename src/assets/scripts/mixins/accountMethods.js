/**
 * Account page methods
 * -----------------------------------------------------------------------------
 *
 * @namespace accountMethods
 */
import axios from 'axios';
import password from 'password-strength-meter';
import 'formdata-polyfill';


const accountMethods = {

    data() {
        return {
            loginFormHelper: {
                status: null,
                message: null,
                customerEmail: '',
            },
        };
    },

    methods: {
        _initAccount() {
            const $accountTabs = $(app.selectors.accountTabs);
            const $loginForm = $(app.selectors.loginForm);
            const $recoverForm = $(app.selectors.recoverForm);
            const $passwordInput = $(app.selectors.passwordInput);
            const hash = window.location.hash;
            let tempCustomerEmail = localStorage.getItem('customer_temp_email');

            if (tempCustomerEmail === 'null') {
                tempCustomerEmail = '';
            }

            app.loginFormHelper.customerEmail = tempCustomerEmail;

            // Submit the form if the email field is not empty
            setTimeout(() => {
                if (app.loginFormHelper.customerEmail !== '') {
                    $('.shopify-warning').hide();
                    app._submitLoginHelperForm();
                }
            }, 500);

            // account tabs => url hash
            hash && $accountTabs.find(`ul.nav a[href="${hash}"]`).tab('show');

            $accountTabs.find('.nav a').click(function (e) {
                window.location.hash = $(this).attr('href');
            });

            function showRecoverPasswordForm() {
                $recoverForm.fadeIn();
                $loginForm.hide();
            }

            function hideRecoverPasswordForm() {
                $recoverForm.hide();
                $loginForm.fadeIn();
            }

            // allow deep linking to the recover password form
            if (hash === '#recover') {
                $accountTabs.find('ul.nav a[href="#login"]').tab('show');
                showRecoverPasswordForm();
            }

            $(document).on('click', '[data-register-link]', () => {
                $accountTabs.find('ul.nav a[href="#register"]').tab('show');
            });

            $(document).on('click', '[data-recover-link]', () => {
                showRecoverPasswordForm();
            });

            $(document).on('click', '[data-cancel-recover-link]', () => {
                hideRecoverPasswordForm();
            });

            // password strength meter
            $passwordInput.password({
                shortPass: 'The password is too short',
                badPass: 'Weak; try combining letters & numbers',
                goodPass: 'Medium; try using special charecters',
                strongPass: 'Strong password',
                containsUsername: 'The password contains the username',
                enterPass: 'Type your password',
                showPercent: false,
                showText: true, // shows the text tips
                animate: true, // whether or not to animate the progress bar on input blur/focus
                animateSpeed: 'fast', // the above animation speed
                username: false, // select the username field (selector or jQuery instance) for better password checks
                usernamePartialMatch: false, // whether to check for username partials
                minimumLength: 6, // minimum password length (below this threshold, the score is 0)
            });

            // toggle collapse for edit address
            // show one card at a time
            $(document).on('click', '[data-edit-address]', function () {
                const target = $(this).data('target');
                $('.collapse').collapse('hide');
                $(target).collapse('show');

                $('html').animate({
                    scrollTop: $(target).offset().top,
                }, 400);

                // unlock window - fix scrolltop common problem
                $(window).bind('mousewheel', () => {
                    $('html').stop();
                });
            });
            app._initShopifyAccountMethods();
        },

        _initShopifyAccountMethods() {
            if ($('[data-account-address]').length === 0) {
                return;
            }
            const addressesIds = JSON.parse($('[data-account-json]').html()).addressesIds;
            new Shopify.CountryProvinceSelector('AddressCountryNew', 'AddressProvinceNew', {
                hideElement: 'AddressProvinceContainerNew',
            });
            addressesIds.forEach((id) => {
                if ($(`#AddressCountry_${id}`).length > 0) {
                    new Shopify.CountryProvinceSelector(`AddressCountry_${id}`, `AddressProvince_${id}`, {
                        hideElement: `AddressProvinceContainer_${id}`,
                    });
                }
            });
            Shopify.CustomerAddress = {
                destroy(id, confirm_msg) {
                    if (confirm(confirm_msg || 'Are you sure you wish to delete this address?')) {
                        Shopify.postLink(`/account/addresses/${id}`, {
                            parameters: {
                                _method: 'delete',
                            },
                        });
                    }
                },
            };
        },

        async _submitLoginHelperForm() {
            
            const formEl = $('#customer_login')[0];
            const formData = new FormData(formEl);
            const customerEmail = formData.get('customer[email]');
            const customerPassword = formData.get('customer[password]');
            localStorage.setItem('customer_temp_email', customerEmail);
            // const response = await axios.get(`/apps/store-utilities-proxy/getCustomerData?customer_email=${customerEmail}`);

            if (!customerEmail) {
                $('#customerEmail').addClass('is-invalid');
                return;
            }

            $('#customerEmail').removeClass('is-invalid');
            app.isLoading = true;

            axios.get(`/apps/store-utilities-proxy/getCustomerData?customer_email=${customerEmail}`)
            .then(function (response) {
                app.isLoading = false;
            
                if (response.data.status == 'disabled' || response.data.status == 'invited') {
                    app.loginFormHelper.status = 'DISABLED';
                    app.loginFormHelper.message = '<p>Account found but it\'s currently <strong>disabled</strong>!</p>We sent you an email with an invitation link. Please follow the instructions.';
    
                } else if (response.data.status == 'enabled') {
                        app.loginFormHelper.status = 'ENABLED';
                        app.loginFormHelper.message = '<p>Account found!</p>Fill in your password in order to login. If you don\'t remember it, use the "Forgot your password?" link below to reset it.';
                    if (customerPassword) {
                        formEl.submit();
                    } else {
                        $('#customerPassword').addClass('is-invalid');
                    }
                } else {
                    app.loginFormHelper.status = 'UNREGISTERED';
                    app.loginFormHelper.message = '<p>No account found.</p>Please select <a href="#register" class="text-primary" data-register-link>"Create an Account"</a> to sign up!';
    
                }
                return response.data;
            })
            .catch(function (error) {
                console.log('error-->', error);
            });

            axios.post('https://s.wlfpt.co/customlogin/get_customer_detail', {
                    shop: 'demo-2005',
                    customer_email: customerEmail,
                })
                .then((response) => {

                    app.isLoading = false;
                    const tag = response.data.tags.split(',');
                    let flag;
                    if (response.data.tags.includes('cj-import') == true || response.data.tags.includes('CJ-IMPORT') == true) {
                        flag = true;
                    } else {
                        flag = false;
                    }
                    
                    // if (flag == true) {
                    //     window.location.replace('https://bloomsybox.cratejoy.com/customer/login');
                    // }else{
                        if (response.data.status == 'disabled' || response.data.status == 'invited') {
                            app.loginFormHelper.status = 'DISABLED';
                            app.loginFormHelper.message = '<p>Account found but it\'s currently <strong>disabled</strong>!</p>We sent you an email with an invitation link. Please follow the instructions.';
    
                        } else if (response.data.status == 'enabled') {
                                app.loginFormHelper.status = 'ENABLED';
                                app.loginFormHelper.message = '<p>Account found!</p>Fill in your password in order to login. If you don\'t remember it, use the "Forgot your password?" link below to reset it.';
                            if (customerPassword) {
                                formEl.submit();
                            } else {
                                $('#customerPassword').addClass('is-invalid');
                            }
                        } else {
                            app.loginFormHelper.status = 'UNREGISTERED';
                            app.loginFormHelper.message = '<p>No account found.</p>Please select <a href="#register" class="text-primary" data-register-link>"Create an Account"</a> to sign up!';
    
                        }
                    // }

                   
                })
                .catch((error) => {
                    console.log('error-->', error);
                });
        },
    },
};

export default accountMethods;