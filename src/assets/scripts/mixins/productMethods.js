/**
 * Product methods
 * -----------------------------------------------------------------------------
 *
 * @namespace productMethods
 */

// Flickity
import Vue from 'vue';
import Flickity from 'flickity';
import 'flickity-as-nav-for';
import 'flickity-fullscreen';
// Product images zoom
import zoomio from 'zoomio';
// Enquire (javascript breakpoints)
import enquire from 'enquire.js';
// Scrollmonitor
import scrollMonitor from 'scrollmonitor';

const productMethods = {

  methods: {
    /**
     * Product Single
     */
    _initProduct() {
      const $container = $(app.selectors.productContainer);
      const $productTabs = $(app.selectors.productTabs);
      const $productToolbarMobile = $(app.selectors.productToolbarMobile);
      const $imageZoom = $(app.selectors.imageZoom);
      const productObj = app.selectors.productObj;
      

      // product tabs add active class to 1st child
      $productTabs.find('.nav-item').first().find('a').addClass('active');
      $productTabs.find('.tab-content .tab-pane').first().addClass('active');

      // Get current default variant fron the product object
      // stop parsing if we don't have the product json script tag when loading
      if (!$(productObj, $container).html()){
        return;
      }

      const standAlone = $('[data-standalone-product-json]').html();
      if(standAlone != undefined){
        app.standalone = JSON.parse(standAlone);
      }
      
      // set data
      app.product = JSON.parse($(productObj, $container).html());
      app.enableHistoryState = $container.data('enable-history-state');
      app.currentProductId = app.product.id;
     
      app._getVariant();

      // Recharge widget
      app._attachRechargeWidget(app.product.id);
  
      function registerMobileToolbar() {
        const productFormPosition = $('[data-form-product]').position();
        const formWatcher = scrollMonitor.create($('[data-form-product]'));
        const footerWatcher = scrollMonitor.create($('[data-footer-bottom]'));
        $productToolbarMobile.show();

        window.addEventListener('scroll', () => {
          const windowScrollY = window.scrollY;

          if (windowScrollY > productFormPosition.top) {
            formWatcher.exitViewport(() => {
              // console.log( 'I have left the viewport' )
              $productToolbarMobile.addClass('semi-opened');
            });

            formWatcher.enterViewport(() => {
              // console.log( 'I have entered the viewport' )
              $productToolbarMobile.removeClass('semi-opened fully-opened');
              if (app.isProductToolbarMobileOpen){
                app.isProductToolbarMobileOpen = false;
              }
            });

            footerWatcher.exitViewport(() => {
              $productToolbarMobile.addClass('semi-opened');
            });

            footerWatcher.enterViewport(() => {
              $productToolbarMobile.removeClass('semi-opened fully-opened');
              if (app.isProductToolbarMobileOpen){
                app.isProductToolbarMobileOpen = false;
              }
            });
          } else {
            $productToolbarMobile.removeClass('semi-opened fully-opened');
            if (app.isProductToolbarMobileOpen){
              app.isProductToolbarMobileOpen = false;
            }
          }
        }); // add scroll event listener
      }

      // mobile product form - sticky toolbar
      enquire.register('screen and (min-width: 720px)', {
        setup() {
          registerMobileToolbar();
        },
        match() {
          $productToolbarMobile.hide();

          $imageZoom.zoomio({
            fadeduration: 200,
          });

          
          $('[data-slider-main-for]').on('beforeChange', (event, slick, currentSlide, nextSlide) => {
            $(`[data-slick-index="${nextSlide}"] img[data-img-zoom]`).zoomio({
                fadeduration: 200,
            });
          });
          
        },
        unmatch() {
          registerMobileToolbar();
        },
      });
    },

    /**
     * Event when product option select box changes
     *
     */

     
    _getVariant(event) {      
      const variant = app._getVariantFromOptions(event);     
      console.log(variant);
      app._getChangeVariantPrice(event, variant.id, variant.price);
      // stop parsing if there is no variant info
      if (!variant){
        return;
      }

      app._updateImages(variant, event);
      app._checkWishlist(variant);
      app._toggleSelectBox(variant);

      if (app.isQuickshopModalOpen) {
        app.quickShop.currentVariant = variant;
      } else {
        app.currentVariant = variant;
        console.log(variant);
        if (app.enableHistoryState){
          app._updateHistoryState(variant);
        }
      }
    },

    /**
     * Toogling select box based on select variant
     */
    _toggleSelectBox(variant){
      const proId = app.currentProductId;
      var original_select = $(`[name=\"id\"][data-productid=\"${proId}\"]`);
     
      const variantTitle = variant.id;
      original_select.val(variantTitle).change();
    },

    /**
     * Get the currently selected options from add-to-cart form.
     *
     * @return {array} options - Values of currently selected variants
     */
    _getCurrentOptions(event) {
      let $container;
      let currentOptionsArr = [];
      const singleOptionSelector = app.selectors.singleOptionSelector;

      if (app.isQuickshopModalOpen) {
        $container = $(app.selectors.quickShopContainer);
      } else if (app.isProductToolbarMobileOpen) {
        $container = $(app.selectors.productToolbarMobile);
      } else {
        $container = $(app.selectors.productContainer);
      }
    
      let currentOptions = $.map($(singleOptionSelector, $container), (element) => {  
        const $element = $(element);
        const type = $element.attr('type');
        const currentOption = {};

        if (type === 'radio' || type === 'checkbox') {
          if ($element[0].checked) {
            currentOption.value = $element.val();
            currentOption.index = $element.data('index');
            currentOptionsArr.push(currentOption);
          }
        }else{
          currentOption.value = $element.val();
          currentOption.index = $element.data('index');
          currentOptionsArr.push(currentOption);
        }       
        
      });
     
      return currentOptionsArr;
      // remove any unchecked input values if using radio buttons or checkboxes
      // currentOptions = app._compact(currentOptions);

      // // remove duplicates
      // currentOptions = currentOptions.filter((item, index, arr) => {
      //   const c = arr.map(item => item.name);
      //   return index === c.indexOf(item.name);
      // });

      // console.log(currentOptions)
    //   return currentOptions;
    },

    /**
     * Find variant based on selected values.
     *
     * @param  {array} selectedValues - Values of variant inputs
     * @return {object || undefined} found - Variant object from product.variants
     */
    _getVariantFromOptions(event) {
      const selectedValues = app._getCurrentOptions(event);
    
      let variants;
      let found = false;

      if (app.isQuickshopModalOpen) {
        variants = app.quickShop.product.variants;
      } else {
        variants = app.product.variants;
      }
     
      variants.forEach((variant) => {
        let satisfied = true;
        const chargeInternalFRQ = $(`[data-${variant.id}-charge-frq-json]`).html();
        const is_hidden = $(`[data-${variant.id}-visible-json]`).html();
        const priceOfEach = variant.price;
        if (is_hidden != undefined){
            Vue.set(variant, 'is_hidden', $.parseJSON($(`[data-${variant.id}-visible-json]`).html()));
        }
        if (chargeInternalFRQ != undefined){
            Vue.set(variant, 'charge_internal_frequency', $.parseJSON($(`[data-${variant.id}-charge-frq-json]`).html()));
            Vue.set(variant, 'each_month', parseInt(priceOfEach / $.parseJSON($(`[data-${variant.id}-charge-frq-json]`).html())));
        }
        
        selectedValues.forEach((option) => {
          if (satisfied){
            satisfied = (option.value === variant[option.index]);
          }
        });
       
        if (satisfied && (variant.is_hidden == false || variant.is_hidden == undefined)){
          found = variant;
        }
      });     
      return found || null;
    },

    /**
     * Update history state for product deeplinking
     *
     * @param  {variant} variant - Currently selected variant
     * @return {k}         [description]
     */
    _updateHistoryState(variant) {
      if (!history.replaceState || !variant){
        return;
      }

      let newurl;

      const showReviewForm = app._getUrlParameter('showReviewForm');

      if (showReviewForm === '') {
        newurl = `${window.location.protocol}//${window.location.host}${window.location.pathname}?variant=${variant.id}`;
      } else {
        newurl = `${window.location.protocol}//${window.location.host}${window.location.pathname}?variant=${variant.id}&showReviewForm=${showReviewForm}`;
      }

      window.history.replaceState({
        path: newurl,
      }, '', newurl);
    },

    /**
     * Find variant image and scroll to this image
     * works for all product page templates
     * can be extended to update all images upon variant selection
     *
     * @param  {variant} variant - Currently selected variant
     */
    _updateImages(variant, event) {
      const variantImage = variant.featured_image || {};
      const variantTitle = variant.title;
      const templateType = $('[data-product-template]').data('product-template');
      const $scrollTarget = $(`[data-variant-title="${variantTitle}"]`);
      const $modal = $(app.selectors.quickShopContainer);

      let $container;
      let currentVariantImage;

      if (app.isQuickshopModalOpen) {
        $container = $(app.selectors.quickShopContainer);
        currentVariantImage = app.quickShop.currentVariant.featured_image || {};
      } else {
        $container = $(app.selectors.productContainer);
        currentVariantImage = app.currentVariant.featured_image || {};
      }

      const $carousel = $container.find(app.selectors.carousel);

      if (!variant.featured_image || variantImage.src === currentVariantImage.src) {
        return;
      }

      enquire.register('screen and (min-width: 720px)', {
        match() {
          // For the 'minimal' product template
          // Scroll to variant image unless it's the first photo on load
          if (templateType !== 'minimal' || !currentVariantImage.src || app.isQuickshopModalOpen){
            return;
          }

          $('html, body').animate({
            scrollTop: $scrollTarget.offset().top,
          }, 350);
        },
      });

      if ($carousel.length > 0) {
        const flkty = Flickity.data($carousel[0]);

        if (typeof flkty === 'undefined'){
          return;
        }

        const cellElements = flkty.getCellElements();

        cellElements.forEach((element, index) => {
          const imgAlt = $(element).data('variant-title');
          if (imgAlt === variantTitle) {
            flkty.selectCell(index, true, false); // value, isWrapped, isInstant
          }
        });
      }
    },
    /**
     * Mobile toolbar - product form - options
     */
    _toggleProductToolbarMobile() {
      const $productToolbarMobile = $(app.selectors.productToolbarMobile);
      app.isProductToolbarMobileOpen = !app.isProductToolbarMobileOpen;
      $productToolbarMobile.toggleClass('fully-opened');
    },

  },
};

export default productMethods;



