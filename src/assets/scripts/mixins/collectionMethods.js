/**
 * Collection methods
 * -----------------------------------------------------------------------------
 *
 * @namespace collectionMethods
 */

import axios from 'axios';
import Flickity from 'flickity';
import createHistory from 'history/createBrowserHistory';

const history = createHistory();

const collectionMethods = {

  methods: {
    _initCollection() {
      const $container = $(app.selectors.collectionContainer);

      // stop parsing if we are not in the collection page
      if (!$container.length > 0) {
        return;
      }

      app._initFilters();
    },

    _showVariant(event) {
      const value = event.target.value;
      const index = event.target.value;
      const $carousel = $(event.target).closest('.card').find('[data-flickity]');

      if ($carousel.length > 0) {
        const flkty = Flickity.data($carousel[0]);
        const cellElements = flkty.getCellElements();

        cellElements.forEach((element, index) => {
          const imgAlt = $(element).data('variant-title');
          if (imgAlt === value) {
            flkty.selectCell(index, true, false); // value, isWrapped, isInstant
          }
        });
      }
    },

    _initFilters() {
      const filtersAjaxed = $(app.selectors.filtersAjaxed).data('filters-ajaxed');
      const location = history.location;
      Shopify.queryParams = {};

      // check collection section settings => enable ajax filters
      if (filtersAjaxed) {
        app.isCollAjaxed = true;
      }

      // Listen for changes to the current location
      history.listen((location, action) => {
        // location is an object like window.location
        // console.log(action, location.pathname, location.state)
        app._runFilters(); // run everytime history changes
      });

      // run one time during 1st page load or upon page refresh
      app._runFilters();
    },

    _runFilters() {
      app._urlParams();
      app._mapFilters();
      const url = app._filtersCreateUrl();
     
      app._getCollProducts(url);
    },

    /**
     * Event when an input filter changes
     *
     */
    _parseFilters(event) {      
      const filterType = $(event.currentTarget).data('filter-type');
      const collectionHandle = $(event.currentTarget).data('handle');
      const selectedTags = [];
      const collTitle = $(event.currentTarget).data('collection-title');
      const colImage = $(event.currentTarget).data('collection-banner');

      // Collection filtering
      if (filterType === 'collection') {
        let newURL = '';
        let search = $.param(Shopify.queryParams);
        let href = $(event.currentTarget).attr('href'); 

        if(href == '#'){
          return false;
        }

        $('[data-site-title]').text(collTitle+' – BloomsyBox');
        $('[data-coll-title]').text(collTitle);
        $('[data-coll-banner]').css('background',"url("+colImage+")");

        if (app.isCollAjaxed) {
          if (!search.length) {
            href += '?';
          }
          app._filterAjaxCall(href);
        } else {
          newURL = href.replace('/collections/','');          
  
          if (selectedTags.length) {
            newURL += `/${selectedTags.join('+')}`;
          }
          delete Shopify.queryParams.page;
          search = $.param(Shopify.queryParams);
          if (search.length) {
            newURL += `?${search}`;
          }
          location.href = newURL;
        }
      }

      // Tag filter
      if (filterType === 'tag') {
        $('[data-filter-tag]').each(function(){
          $(this).find('[data-filter]:checked').each(function(i) {
            selectedTags.push(this.value);
          });        
        })

        app.activeFilters = selectedTags;

        if (selectedTags.length) {
          Shopify.queryParams.constraint = selectedTags.join('+');
        } else {
          delete Shopify.queryParams.constraint;
        }

        if (app.isCollAjaxed) {
          app._filterAjaxCall();
        } else {
          delete Shopify.queryParams.page;
          location.search = $.param(Shopify.queryParams).replace(/%2B/g, '+');
        }

      }

      // Sort filter
      if (filterType === 'sort') {
        const sortValue = $(event.currentTarget).val();

        if (sortValue) {
          Shopify.queryParams.sort_by = sortValue;
        } else {
          delete Shopify.queryParams.sort_by;
        }

        if (app.isCollAjaxed) {
          app._filterAjaxCall();
        } else {
          const search = $.param(Shopify.queryParams);
          if (search.length) {
            location.search = $.param(Shopify.queryParams);
          } else {
            location.href = location.pathname;
          }
        }
      }

      // Collection pagination
      if (filterType === 'pagination') {
        let url = $(event.currentTarget).attr('href').match(/page=\d+/g);

        if (url && (Shopify.queryParams.page === parseInt(url[0].match(/\d+/g), 10), Shopify.queryParams.page)) {
          if (app.isCollAjaxed) {
            url = app._filtersCreateUrl();
            history.push(url, {
              param: Shopify.queryParams,
            });
            app._getCollProducts(url);
          }
        }
      }

      // map all filters
      app._mapFilters();
    },

    /**
     * Get all parameters from URL and map them to all inputs in page
     *
     */
    _mapFilters() {
      // Read url parameters and map keys to filters
      if (location.search.length) {
        for (let aKeyValue, i = 0, aCouples = location.search.substr(1).split('&'); i < aCouples.length; i += 1) {
          aKeyValue = aCouples[i].split('=');
          if (aKeyValue.length > 1) {
            Shopify.queryParams[decodeURIComponent(aKeyValue[0])] = decodeURIComponent(aKeyValue[1]);
          }
        }
      }

      // sorting: select value
      if (Shopify.queryParams.sort_by) {
        $('[data-filter-sort]').val(Shopify.queryParams.sort_by);
      } else {
        $('data-filter-sort').val('manual');
      }

      // tags: select values
      if (Shopify.queryParams.constraint) {
        const selectedTags = [];
        $.each(Shopify.queryParams.constraint.split('+'), (i, val) => {
          if (val) {
            selectedTags.push(val);
          }
        });

        app.activeFilters = selectedTags;
      }
    },

    _filtersCreateUrl(e) {      
      const b = $.param(Shopify.queryParams).replace(/%2B/g, '+');
      return e ? b !== '' ? `${e}?${b}` : e : `${location.pathname}?${b}`;
    },

    _filterAjaxCall(e) {      
      delete Shopify.queryParams.page;
      const url = app._filtersCreateUrl(e);      
      history.push(url, {
        param: Shopify.queryParams,
      });
      app._getCollProducts(url);
    },

    _urlParams() {
      if (Shopify.queryParams === {}, location.search.length) {
        for (let i, e = 0, t = location.search.substr(1).split('&'); e < t.length; e += 1) {
          i = t[e].split('='), i.length > 1 && (Shopify.queryParams[decodeURIComponent(i[0])] = decodeURIComponent(i[1]));
        }
      }
    },

    _clearAllFilters() {
      delete Shopify.queryParams.constraint;
      delete Shopify.queryParams.q;
      app.activeFilters = [];            

      $('[data-coll-title]').text($('[data-coll-title]').attr('data-current-coll'));

      if (app.isCollAjaxed) {
        $('[data-filter]').attr('checked', false);
        $('[data-filter]').prop('checked', false);
        // app._filterAjaxCall();
        delete Shopify.queryParams.page;
        const b = $.param(Shopify.queryParams).replace(/%2B/g, '+');
        const url =  "/collections/"+$('body').attr('data-current-coll');
        history.push(url, {
          param: Shopify.queryParams,
        });
        app._getCollProducts(url);
      } else {
        location.search = $.param(Shopify.queryParams);        
      }
    },

    _getCollProducts(url) {
      if (!app.isCollAjaxed) {
        return;
      }

      const search = $.param(Shopify.queryParams);
      app.collProducts = [];
      app.isLoading = true;

      url += (search.length) ? '&view=products.json' : 'view=products.json';

      // for Shopify theme editor
      if (Shopify.designMode) {
        url = '/admin/products.json?limit=12';
      }

      axios.get(url)
        .then((response) => {
          app.isLoading = false;

          if (Shopify.designMode) {
            app.collProducts = response.data.products;
          } else {
            app.collProducts = response.data;
          }

          app._getCollPagination(url);
          // console.log(app.collProducts)

          // reinitialize product reviews for each grid item
          app._getCollectionReviews();

          /*
          $('html').animate({
            scrollTop: $('html').offset().top
          }, 400)
          // unlock window - fix scrolltop common problem
          $(window).bind('mousewheel', function() {
            $('html').stop()
          })
          */
        })
        .catch((error) => {
          // console.log(error);
        });
    },    

    _getCollPagination(url) {
      url += '&view=paginate.json';

      axios.get(url)
        .then((response) => {
          app.collPagination = response.data;
        })
        .catch((error) => {
          // console.log(error);
        });
    },

    _loadMore(event) {
      let url = $(event.currentTarget).attr('href');
      url = url.replace('paginate', 'products');
      app.isLoading = true;

      axios.get(url)
        .then((response) => {
          $.each(response.data, (key, obj) => {
            app.collProducts.push(obj);
          });

          app.isLoading = false;
          app._getCollPagination(url);
        })
        .catch((error) => {
          // console.log(error);
        });
    },

  },
};

export default collectionMethods;