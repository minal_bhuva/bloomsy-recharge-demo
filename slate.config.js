/* eslint-disable */

// Configuration file for all things Slate.
// For more information, visit https://github.com/Shopify/slate/wiki/Slate-Configuration

const webpack = require('webpack');
const path = require('path');

module.exports = {
  'cssVarLoader.liquidPath': ['src/snippets/css-variables.liquid'],
  'webpack.extend': {
    resolve: {
      alias: {
        'lodash-es': path.resolve('./node_modules/lodash-es'),
        vue$: path.resolve('./node_modules/vue/dist/vue.esm.js'),
        jquery: path.resolve('./node_modules/jquery'),
      },
    },
    module: {
      rules: [
        {
          test: /\.js$/,
          use: 'imports-loader?$=jquery,jQuery=jquery',
        },
        {
          test: /\.(ttf|eot|svg|otf|webp)(\?v=[0-9]\.[0-9]\.[0-9])?$/,
          use: [
            {
              loader: 'file-loader',
            },
          ],
        },
        {
          test: /\.woff(2)?(\?v=[0-9]\.[0-9]\.[0-9])?$/,
          use: [
              {
                  loader: 'url-loader',
                  options: {
                      limit: 1000,
                      mimetype: 'application/font-woff'
                  }
              }

          ]
        },
      ],
    },
    plugins: [
      new webpack.IgnorePlugin(/^\.\/locale$/, /moment$/),
    ],
  },
};
