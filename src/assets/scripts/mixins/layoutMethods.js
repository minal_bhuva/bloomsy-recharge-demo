/**
 * Layout related methods
 * -----------------------------------------------------------------------------
 *
 * @namespace layoutMethods
 */

import enquire from 'enquire.js';
import detectIt from 'detect-it';
import tinycolor from 'tinycolor2';
import Flickity from 'flickity';
import YouTubeIframeLoader from 'youtube-iframe';

let videoPlayer = null;

const layoutMethods = {

  methods: {
    _initLayout() {
      window.addEventListener('scroll', app._checkScrollPos); // add scroll event listener
      window.addEventListener('scroll', app._onScroll);
      // make all images in RTE editor load via lazyload
      $('.rte img').addClass('lazyload');
      jQuery(window).resize(() => {
          app._initStickyHeader();
          app._checkScrollPos();
      });

      app._initBootstrapComponents();
      app._initNewsletterPopup();
      app._initStickyHeader();
      app._initCarousels();
      // app._initInstafeed();
      app._checkScrollPos();
      app._holidaysDate();
    },

    _initBootstrapComponents() { /* Bootstrap components that need to be initialized should be called here */
      // init popover
      $('[data-toggle="popover"]').popover();

      // cart dropdown popover
      $('[data-cart-dropdown]').popover({
        html: true,
        content() {
          return $(this).next('[data-cart-dropdown-content]').html();
        },
      });

      // init tooltip but only for desktop
      if (!detectIt.hasTouch) {
        $('[data-toggle="tooltip"]').tooltip();
      }

      // make navigation dropdowns open on mouse hover instead of click
      $('.navbar a[data-toggle="dropdown"]').on('mouseenter', function (e) {
        const $dropdownLink = $(this).parent();
        const $dropdownMenu = $dropdownLink.find('.dropdown-menu');

        $dropdownLink.addClass('show');
        $dropdownMenu.addClass('show');

        $dropdownLink.on('mouseleave', function (e) {
          $(this).removeClass('show');
          $dropdownMenu.removeClass('show');
        });
      });
    },

    _initNewsletterPopup() {
      const customerPosted = (window.location.href.search('[?&]customer_posted=true') !== -1);
      const $newsletterPopup = $(app.selectors.newsletterPopup);
      const popupDelay = $newsletterPopup.data('popup-delay') * 1000;
      const daysUntil = $newsletterPopup.data('period-until');
      const newsletterPopupEnabled = $newsletterPopup.data('newsletter-enabled');
      const newsletterPopupStatus = app._getCookie('newsletter_popup_status');
      const today = new Date();
      const expirationDate = new Date();
      expirationDate.setDate(today.getDate() + daysUntil);
      const time = expirationDate.getTime();
      const expireTime = time + 1000 * 36000;
      expirationDate.setTime(expireTime);

      // after registration get the 'customer_posted' parameter from URL
      // show popup after successfull registration
      // the popup will have the 'Thank you' message after registration redirection
      if (customerPosted) {
        app._toggleNewsletterPopup();
      }

      // do not show the popup if the disabled status cookie exists
      if (newsletterPopupStatus === 'disabled' || !newsletterPopupEnabled){
        return;
      }

      setTimeout(() => {
        app._toggleNewsletterPopup();
      }, popupDelay);

      $(document).on('click', '[data-newsletter-close-btn]', () => {
        document.cookie = `newsletter_popup_status=disabled;expires=${expirationDate.toGMTString()};path=/`;
      });
    },

    _initStickyHeader() {
      const $siteHeader = $(app.selectors.siteHeader);
      const $headerSlider = $(app.selectors.headerSlider);
      const $mainNav = $siteHeader.find('nav');
      const navHeight = $mainNav.outerHeight() - 3;
      const colBarHeight = $('[data-collection-bar]').outerHeight();

      if ($siteHeader.data('fixed-header')) { // boolean
        //$('body:not(.template-index) #app').css('padding-top', navHeight);
        if ($('[data-collection-bar]').length > 0){
          const topOffset = navHeight + colBarHeight;
          $('body #app').css('padding-top', topOffset);          
          if($('body #app .collection-filters').length > 0)
            $('body #app .collection-filters').css('top', topOffset);
        //   $('body').scrollspy({
        //       target: '#navbar-example2',
        //       offset: topOffset + 40,
        //   });
        $('.nav-pills-custom').find('a').click(function(e) {
            e.preventDefault();
            $(document).off('scroll');
            const section = $(this).attr('href');
            $('.nav-pills-custom a').removeClass('active');
            $(this).addClass('active');
            $('html, body').animate({
                scrollTop: $(section).offset().top - (topOffset + 10),
            });
        });

          $('[data-collection-bar]').css('top', navHeight);
         // app._collectionScroll();
        } else {
          $('body #app').css('padding-top', $siteHeader.outerHeight());
          if($('body #app .collection-filters').length > 0)
            $('body #app .collection-filters').css('top', $siteHeader.outerHeight());
        }
      } else {
        $headerSlider.css('height', $headerSlider.outerHeight() - navHeight);
      }
    },

    _onScroll(event){
        const windowScrollY = window.scrollY;
        const $siteHeader = $(app.selectors.siteHeader);
        const $headerSlider = $(app.selectors.headerSlider);
        const $mainNav = $siteHeader.find('nav');
        const colBarHeight = $('[data-collection-bar]').outerHeight();
        const navHeight = $mainNav.outerHeight() - 3;
        const topOffset = navHeight + colBarHeight + 15;
        $('.nav-pills-custom a').each(function(){
            const currLink = $(this);
            const refElement = $(currLink.attr('href'));
            if (refElement.position().top - topOffset <= windowScrollY && refElement.position().top + refElement.height() > windowScrollY) {
                $('.nav-pills-custom a').removeClass('active');
                currLink.addClass('active');
            } else {
                currLink.removeClass('active');
            }
        });
    },


    /**
     * Window scroll info => update classes & elements
     */
    _checkScrollPos() {
      const $siteHeader = $(app.selectors.siteHeader);
      const $mainNav = $siteHeader.find('nav');
      const $navBgColor = $mainNav.css('backgroundColor');
      const isHeaderTransparent = $siteHeader.data('transparent-header');
      const isDarkBg = tinycolor($navBgColor).isDark(); // returns boolean
      const isHomepage = $('body').hasClass('template-index');
      const windowScrollY = window.scrollY;

      if (app.scrollPosition < windowScrollY && app.scrollPosition > 10) {
        // direction down
        $('body').removeClass('is-scrolled-top is-scrolled-up').addClass('is-scrolled-down');

        if (!isDarkBg && isHeaderTransparent) {
          app.isLogoLight = false;
        }
      } else {
        // direction up
        $('body').removeClass('is-scrolled-top is-scrolled-down').addClass('is-scrolled-up');
      }

      if (windowScrollY <= 50) {
        // reached top
        $('body').removeClass('is-scrolled-up').addClass('is-scrolled-top');

        if (isHomepage && isHeaderTransparent) {
          app.isLogoLight = true;
        }
      }

      // hide popover past to 200
      // if (app.scrollPosition > 200){
      //   app.$root.$emit('bv::hide::popover')
      // }

      app.scrollPosition = windowScrollY;
    },

    _initCarousels() {
      const $carousel = $(app.selectors.carousel);

      if ($carousel.length > 0) {
        const flkty = Flickity.data($carousel[0]); // pass in the element, $element[0], not the jQuery object

        if (typeof flkty === 'undefined'){
          return;
        }

        const totalSlides = flkty.cells.length;
        let selectedSlide = flkty.selectedElement;
        let slideType = $(selectedSlide).data('slide-type');

        if (totalSlides <= 1) {
          $carousel.addClass('hide-nav-ui');
        }

        flkty.on('settle', () => {
          selectedSlide = flkty.selectedElement;
          slideType = $(selectedSlide).data('slide-type');

          if (slideType === 'video') {
            if (videoPlayer) {
              videoPlayer.playVideo();
            } else {
              app._initVideo(selectedSlide);
            }
          } else if (videoPlayer) {
            videoPlayer.pauseVideo();
          }
        });

        flkty.resize();
        app._triggerResize();
      }

      // init recently viewed carousel
      setTimeout(() => {
        const $carouselAjax = $(app.selectors.carouselAjax);

        if ($carouselAjax.length > 0) {
          const options = $carouselAjax.data('flickity-ajax');
          const flkty = new Flickity($carouselAjax[0], options);
          flkty.resize();
        }
      }, 100);
    },

    _initVideo(selectedSlide) {
      const $selectedVideo = $(selectedSlide).find('[data-video-slide]');
      const selectedVideoId = $selectedVideo.data('video-id');

      if (!selectedVideoId){
        return;
      }

      function onPlayerReady(e) {
        e.target.setPlaybackQuality('hd720');
        e.target.mute();
        e.target.playVideo();
      }

      function onPlayerStateChange(e) {
        if (e.data === YT.PlayerState.ENDED) {
          // console.log('video finished');
        }
      }

      enquire.register('screen and (min-width: 720px)', {
        match() {
          YouTubeIframeLoader.load((YT) => {
            videoPlayer = new YT.Player($selectedVideo[0], {
              videoId: selectedVideoId,
              width: '100%',
              height: '100%',
              playerVars: {
                enablejsapi: 1,
                autoplay: 1,
                loop: 1,
                playlist: selectedVideoId, // otherwise loop will not work
                rel: 0,
                controls: 0,
                showinfo: 0,
                modestbranding: 0,
                disablekb: 1,
                // 'start': 0,
                // 'end': 335,
              },
              events: {
                onReady: onPlayerReady,
                onStateChange: onPlayerStateChange,
              },
            });
          });
        },
        unmatch() {
          if (videoPlayer){
            videoPlayer.destroy();
          }
        },
      });
    },

    // _initInstafeed() {
    //   const instagramAccessToken = $(app.selectors.instafeed).data('instagram-access-token');

    //   // Instafeed (refer to 'http://instafeedjs.com/' for full options)
    //   if (instagramAccessToken) {
    //     const feedLimit = $('[data-feed-limit]').data('feed-limit');
    //     const instafeed = new Instafeed({
    //       accessToken: instagramAccessToken,
    //       limit: feedLimit,
    //       sortBy: 'most-recent',
    //       imageTemplate: '<div class="col-6 col-md-3 inst-filed-col"><a href="{{link}}" class="inst-filed shadow-1"><div style="background-image: url({{image}})" class="inst-img position-absolute w-100 h-100 t-0 l-0"></div><div class="instgram-text d-flex align-items-center justify-content-center position-absolute w-100 h-100 t-0 l-0"><div class="p-6 text-center"><div class="d-flex align-items-center justify-content-center mb-4"><h5 class="text-white mb-0 mr-4"><i class="far fa-heart mr-2"></i> {{likes}}</h5><h5 class="text-white mb-0"><i class="far fa-comment mr-2"></i> {{comments}}</h5></div><p class="mb-0 text-white">{{caption}}</p></div></div></a></div>',
    //       carouselFrameTemplate: '<div class="col-6 col-md-3 inst-filed-col"><a href="{{link}}" class="inst-filed shadow-1"><div style="background-image: url({{previewImage}})" class="inst-img"></div><div class="instgram-text d-flex align-items-center justify-content-center position-absolute w-100 h-100 t-0 l-0"><div class="p-6 text-center"><div class="d-flex align-items-center justify-content-center mb-4"><h5 class="text-white mb-0 mr-4"><i class="far fa-heart mr-2"></i> {{likes}}</h5><h5 class="text-white mb-0"><i class="far fa-comment mr-2"></i> {{comments}}</h5></div><p class="mb-0 text-white">{{caption}}</p></div></div></a></div>',
    //       imageResolution: 'standard-resolution',
    //       videoTemplate: '<div class="instagram-item col-4 col-md-2"><a href="{{link}}" target="_blank" title="IZZE" rel="noreferrer"><div class="overlay d-flex align-items-center justify-content-center"><i class="ion-social-instagram"></i></div><img src="{{previewImage}}" class="img-fluid"/></a></div>',

    //       // filter(data) {
    //       //   return data.tags.find(function(tag) {
    //       //     return tag === "awesome"
    //       //   })
    //       // },

    //       onSuccess() {},
    //       onError(message) {},
    //     });
    //     instafeed.run();
    //   }
    // },

    /**
     * Event when variant quantity selector changes
     */
    _updateQty(event) {
      const $qtyEl = $(event.currentTarget).parentsUntil(app.selectors.qtyContainer).find(app.selectors.qtyInput);
      let qtyValue = $qtyEl.val();

      if ($(event.currentTarget).data('qty-decrease') !== undefined) {
        if (parseFloat(qtyValue) >= 2){
          qtyValue = parseFloat(qtyValue) - 1;
        }
      } else if ($(event.currentTarget).data('qty-increase') !== undefined) {
        qtyValue = parseFloat(qtyValue) + 1;
      }

      $qtyEl.val(qtyValue);
    },

    /**
     * Event for toggling the cart drawer
     *
     */
    _toggleCartDrawer() {
      const $siteOverlay = $(app.selectors.siteOverlay);
      const cartType = $(app.selectors.siteHeader).data('cart-type');

      // stop parsing if cart type is not set to 'drawer'
      if (cartType !== 'cart_drawer'){
        return;
      }

      if (!app.isCartDrawerOpen) { // open drawer
        app.isOverlayVisible = true;
        app._lockScroll();

        // re-init tooltips but only for desktop
        if (!detectIt.hasTouch) {
          $('[data-toggle="tooltip"]').tooltip();
        }
      } else {
        app.isOverlayVisible = false;
        app._unlockScroll();
      }

      // toggle the status data
      app.isCartDrawerOpen = !app.isCartDrawerOpen;

      $siteOverlay.on('click', () => {
        if (app.isSearchLayerOpen){
          return;
        }
        app._toggleCartDrawer();
      });

      $(document).on('keydown', (e) => { // close drawer
        if (!app.isCartDrawerOpen){
          return;
        }
        e = e || window.event;
        if (e.keyCode === 27){
          app._toggleCartDrawer();
        }
      });
    },

    _toggleCartDrawerAddOns() {
      const $siteOverlay = $(app.selectors.siteOverlay);
      const cartType = $(app.selectors.siteHeader).data('cart-type');

      if (!app.isCartDrawerAddOnsOpen) { // open drawer
        app.isOverlayVisible = true;
        app._lockScroll();

        // re-init tooltips but only for desktop
        if (!detectIt.hasTouch) {
          $('[data-toggle="tooltip"]').tooltip();
        }
      } else {
        app.isOverlayVisible = false;
        app.isaddedTocart = false;
        $('.cartaddedShow').removeClass('d-none');
        $('.cartAddedRemove').addClass('d-none');
        app.selectedAddOns.selectProduct = [];
        app.recentlyCart = [];
        const priceEle = app.selectors.addOnsPrice;
        $(priceEle).attr('data-priceVal', app.product.price);
        app.selectedAddOns.totalPrice = app.product.price;
        //app.recentlyCart = null;
        app._unlockScroll();
      }

      // toggle the status data
      app.isCartDrawerAddOnsOpen = !app.isCartDrawerAddOnsOpen;

      $siteOverlay.on('click', () => {
        if (app.isSearchLayerOpen){
          return;
        }
        app._toggleCartDrawerAddOns();
      });

      $(document).on('keydown', (e) => { // close drawer
        if (!app.isCartDrawerAddOnsOpen){
          return;
        }
        e = e || window.event;
        if (e.keyCode === 27){
          app._toggleCartDrawerAddOns();
        }
      });
    },

    /**
     * Event for toggling the mobile menu drawer
     *
     */
    _toggleMobileNavDrawer() {
      const $siteOverlay = $(app.selectors.siteOverlay);

      if (!app.isMobileNavDrawerOpen) { // open drawer
        app.isOverlayVisible = true;
        app._lockScroll();
      } else {
        app.isOverlayVisible = false;
        app._unlockScroll();
      }

      // toggle the status data
      app.isMobileNavDrawerOpen = !app.isMobileNavDrawerOpen;

      $siteOverlay.on('click', () => {
        if (!app.isMobileNavDrawerOpen){
          return;
        }
        app._toggleMobileNavDrawer();
      });
    },

    /**
     * Event for toggling the search layer
     *
     */
    _toggleSearch() {
      const $searchLayer = $(app.selectors.searchLayer);
      const searchType = $searchLayer.data('search-type');
      const $inputSearch = $searchLayer.find('#searchInput');

      if (!app.isSearchLayerOpen) {
        $inputSearch.val('');
        app._lockScroll();

        if (searchType === 'full_screen') {
          $('header, footer, section').addClass('filter-blur').css('opacity', '0.5');
          $('[data-product-toolbar-mobile]').hide();
        } else {
          app.isOverlayVisible = true;
        }

        setTimeout(() => {
          $inputSearch.focus();
        }, 500);
      } else {
        $inputSearch.blur();
        app._unlockScroll();

        if (searchType === 'full_screen') {
          $('header, footer, section').removeClass('filter-blur').css('opacity', '1');
          $('[data-product-toolbar-mobile]').show();
        } else {
          app.isOverlayVisible = false;
        }
      }

      // toggle the status data
      app.isSearchLayerOpen = !app.isSearchLayerOpen;

      $(document).on('keydown', (e) => { // close drawer
        if (!app.isSearchLayerOpen){
          return;
        }
        e = e || window.event;
        if (e.keyCode === 27 && app.isSearchLayerOpen){
          app._toggleSearch();
        }
      });
    },

    /**
     * Event for toggling the newsletter popup
     *
     */
    _toggleNewsletterPopup() {
      if (!app.isNewsletterPopupOpen) {
        app._lockScroll();
        app.isOverlayVisible = true;
      } else {
        app._unlockScroll();
        app.isOverlayVisible = false;
      }

      // toggle the status data
      app.isNewsletterPopupOpen = !app.isNewsletterPopupOpen;
    },

    /**
     * Lock window scroll
     *
     */
    _lockScroll() {
      $('html').css('overflow', 'hidden').addClass('popup-opend');

      $(document).on('touchmove', (e) => {
        if (!$(e.target).closest('[data-touch-moveable]').length) {
          e.preventDefault();
        }
      });
    },

    /**
     * Unlock window scroll
     *
     */
    _unlockScroll() {
      $('html').css('overflow', 'auto').removeClass('popup-opend');
      $(document).unbind('touchmove');
    },

    /**
     * Holidays OF USA
     *
     */
    _holidaysDate(){
      const container = app.selectors.holidaysDate;
      const holidaysVal = $(container).val();
      if ($(container).length > 0){
        const singleVal = holidaysVal.split(',');
        const propValues = [];
        for (let i, e = 0, t = singleVal; e < t.length; e += 1) {
          propValues[e] = t[e];
        }      
        app.holidays = propValues;
      }
    },


    _collectionScroll(){
        $('.collection-filter ul li > a, .section-scroll a').on('click', function(event) {
            if (this.hash !== '') {
              event.preventDefault();
              const hash = this.hash;
              const topOffset = $(hash).offset().top - 150;
              $('html, body').animate({
                scrollTop: topOffset,
              }, 600);
            }
        });
    },

    _attachRechargeWidget(productId) {
      
      // recharge widget
      const $subscriptionWidget = $(`[data-recharge-subscription-widget="${productId}"]`);
      const $rcContainer = $(`#recurring_choice_${productId}`);
     
      if ($rcContainer.length > 0) {
        $subscriptionWidget.find('.inner').append($rcContainer);
        $subscriptionWidget.show();
        $rcContainer.show();
      }
    },

    /**
     * Recharge Integration => Detach Recharge widget dynamically
     */
    _detachRechargeWidget(productId) {
      // recharge widget
      const $subscriptionWidget = $(`[data-recharge-subscription-widget="${productId}"]`);
      const $rcContainer = $(`#recurring_choice_${productId}`);
      const $ghostPlaceholder = $('[data-recharge-ghost-placeholder]');

      if ($rcContainer.length > 0) {
        // reset recharge widget form to first options
        $('[data-input-one-time]').trigger('click');
        $subscriptionWidget.find('select').prop('selectedIndex', 0);
        // remove subscription properties
        $('[name="properties[subscription_id]"]').remove();
        // put back
        $ghostPlaceholder.append($rcContainer);
        $subscriptionWidget.find('.inner').empty();
        $subscriptionWidget.hide();
        $rcContainer.hide();
      }
    },

  },
};

export default layoutMethods;
